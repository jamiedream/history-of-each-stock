import datetime
import time
import requests
from bs4 import BeautifulSoup as bs
from requests.api import patch

import config

response = requests.get(config.target_url, headers=config.headers)
# print(response.text)
soup = bs(response.text, 'lxml')


def time_in_range(date_str: str, start, end):
    date = time.strptime(date_str, '%Y-%m-%d').tm_yday
    in_range = False
    if date >= start and date <= end:
        in_range = True
    return in_range

# date format yyyy-MM-dd


def get_data(start_time, end_time):
    start = time.strptime(start_time, '%Y-%m-%d').tm_yday
    end = time.strptime(end_time, '%Y-%m-%d').tm_yday
    history_data = []
    table = soup.find('table', attrs={'id': 'curr_table'}).find_all('tr')
    for tr in table:
        td = tr.find_all('td')[0:2]
        row = {}
        date = ''
        price = ''
        for d in td:
            text = d.text.strip()
            if "年" in text:
                date = datetime.datetime.strptime(
                    text, '%Y年%m月%d日').strftime('%Y-%m-%d')
                if time_in_range(date, start, end):
                    row['date'] = date
                else:
                    break
            else:
                price = text
                row['price'] = price
        # print(row)
        if row != {}:
            history_data.append(row)
    # print(history_data)
    return history_data


print(get_data('2021-07-19', '2021-07-23'))
